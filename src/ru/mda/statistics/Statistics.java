package ru.mda.statistics;

import java.io.*;
import java.util.Scanner;

/**
 * Класс выполняет подсчёт статистики
 *
 * @author Макаров.Д.А 17ИТ18
 */
public class Statistics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь к файл из которого будут подсчитываться символы");
        String pathInput = scanner.nextLine();
        System.out.println("Как производить запись статистики? Если вы хотите вывод в консоль введите 1, если в файл 2");
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
            outputConsole(fillsArrStatistics(inputFile(pathInput)));
            break;
            case 2:
            System.out.println("Введите путь к файлу в который нужно записать статистику");
            outputFile(fillsArrStatistics(inputFile(pathInput)));
            break;
            default:
                System.out.println("Ввод не коректных данных");
        }
    }

    /**
     * Метод считывает данные с файла
     *
     * @param path - путь к файлу
     * @return считываемый текст из файла
     */
    private static String inputFile(String path) {
        StringBuilder line = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                line.append(read);
                line.append(" ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line.toString();
    }

    /**
     * Метод выполняет подсчёт символов в тексте
     *
     * @param line - текст
     * @return количество символов в тексте
     */
    public static int CountSymbol(String line) {
        return line.length();
    }

    /**
     * Метод выполняет подсчёт пробелов в тексте
     *
     * @param line - текст
     * @return количество пробелов в тексте
     */
    public static int CountSpace(String line) {
        int numberSpace = 0;
        String delimetrSymbol = "";
        String[] arrSymbol;
        arrSymbol = line.split(delimetrSymbol);
        for (int i = 0; i < arrSymbol.length; i++) {
            if (arrSymbol[i].matches("\\s")) {
                numberSpace++;
            }
        }
        return numberSpace;
    }

    /**
     * Метод выполняет подсчёт слов в тексте
     *
     * @param line - текст
     * @return количество слов в тексте
     */
    public static int CountWorld(String line) {
        int numberWords = 0;
        String[] arrWords;
        String delimetrWords = " ";
        arrWords = line.split(delimetrWords);
        for (int i = 0; i < arrWords.length; i++) {
            if (!arrWords[i].matches("[-\\s]+")) {
                numberWords++;
            }
        }
        return numberWords;
    }

    /**
     * Метод заполняет массив статистики данными
     *
     * @param line - текст
     * @return массив с статистикой
     */
    private static int[] fillsArrStatistics(String line) {
        int[] statistic = { CountSymbol(line),  CountSymbol(line) - CountSpace(line), CountWorld(line)};
        return statistic;
    }


    /**
     * Метод выполняет вывод в консоль
     *
     * @param statistic - массив данных с статистикой
     */
    private static void outputConsole(int[] statistic) {
        System.out.print("Статистика: \nСимволов всего: " + statistic[0] + "\nСимволов без пробела: " + statistic[1] + "\nСлов: " + statistic[2]);
    }

    /**
     * Метод выполняет запись в файл
     *
     * @param statistic - массив с статистикой
     */
    private static void outputFile(int[] statistic) {
        Scanner scanner = new Scanner(System.in);
        String pathOutput = scanner.nextLine();
        try (FileWriter writer = new FileWriter(pathOutput, false)) {
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write("Статистика: \nКоличество символов: " + statistic[0] + "\n" + "Количество символов без пробела: " + statistic[1] + "\nКоличество слов: " + statistic[2]);
            bufferedWriter.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

//src//ru//mda//statistics//file//input.txt
//src//ru//mda//statistics//file//output.txt

