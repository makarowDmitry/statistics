package ru.mda.statistics;

import org.junit.Test;

import static org.junit.Assert.*;

public class StatisticsTest {
    private String line = "Привет, сколько лет, сколько зим?";

    @Test
    public void countSymbol() {
        int symbol = 33;
        assertEquals(symbol, Statistics.CountSymbol(line));
    }

    @Test
    public void countSpace() {
        int space = 4;
        assertEquals(space, Statistics.CountSpace(line));
    }

    @Test
    public void countWorld() {
        int world = 5;
        assertEquals(world, Statistics.CountWorld(line));
    }
}